require 'http'

module Gateline
  class HTTPS
    URL_TEST = "https://api.sandbox.gateline.net:18210"
    URL_SIMPLE = "https://api.sandbox.gateline.net:18220"
    URL = "https://api.gateline.net"

    def initialize(key_path, crt_path, identifer, password, debug = true)
      @debug = debug
      @identifer = identifer
      @password = password
      @ctx      = OpenSSL::SSL::SSLContext.new
      @ctx.cert = OpenSSL::X509::Certificate.new(File.read(crt_path))
      @ctx.key  = OpenSSL::PKey::RSA.new(File.read(key_path))
    end

    def test_ping
      url = '/test/ping'
      get_request(url)
    end

    def get_request(url, h = {})
      digest = Digest.make(url, h, @password)
      res_url = current_url + url
      HTTP.headers(headers(digest))
        .get(res_url, :ssl_context => @ctx)
    end

    def post_request(url, h = {})
      digest = Digest.make(url, h, @password)
      res_url = current_url + url
      HTTP.headers(headers(digest)).post(res_url, params: h, :ssl_context => @ctx)
    end

    def headers(digest)
      r = {
        'X-Authorization' => [@identifer, digest].join(' ')
      }
      r
    end

    def current_url
      if @debug
        URL_TEST
      else
        URL
      end
    end
  end
end
