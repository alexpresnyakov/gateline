require "openssl"

module Gateline
  module Digest
    module_function

    def make(path, h, key)
      arr = h.keys.sort.map do |k|
        '%s=%s' % [k, h[k]]
      end
      arr.unshift path
      arr << '' if arr.size == 1
      str = arr.join(';')

      digest = OpenSSL::Digest.new('sha1')
      OpenSSL::HMAC.hexdigest(digest, key, str)
    end
  end
end
