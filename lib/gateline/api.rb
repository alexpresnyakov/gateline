require 'nokogiri'
module Gateline
  class Api
    def initialize(k_path, crt_path, identifer, key, debug = true)
      @https = HTTPS.new(k_path, crt_path, identifer, key, debug)
    end

    def activation(params)
      n = Nokogiri::Slop( @https.post_request("/checkout/activation", params).body.to_s )
      # n.checkout.activation.redirect.content
      Hash.from_xml(n.to_xml)
    end

    def checkout_pay(params)
      n = Nokogiri::Slop(@https.post_request("/checkout/pay", params).body.to_s)
      # n.checkout.pay.redirect.content
      Hash.from_xml(n.to_xml)
    end

    def rebill(order_id, params)
      body = @https.post_request("/order/#{order_id}/rebill", params).body.to_s
      n = Nokogiri::Slop(body)
      Hash.from_xml(n.to_xml)
    end

    def rebill3d(order_id, params)
      body = @https.post_request("/order/#{order_id}/rebill3d", params).body.to_s
      n = Nokogiri::Slop(body)
      Hash.from_xml(n.to_xml)
    end

    def order(order_id)
      body = @https.get_request("/order/#{order_id}").body.to_s
      n = Nokogiri::Slop(body)
      Hash.from_xml(n.to_xml)
    end
  end
end
