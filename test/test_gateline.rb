require 'helper'
require 'pp'

class TestGateline < Test::Unit::TestCase
  should "initialize https" do
    key_path = "#{File.expand_path('.')}/test/certs/pixellaru.api.key.pem"
    crt_path = "#{File.expand_path('.')}/test/certs/pixellaru.api.crt.pem"

    https = Gateline::HTTPS.new(key_path, crt_path, 'pixellaru', '1379364a819506583e06628f28a686d3')
    
    # puts https.test_ping
  end

  should 'api initialize' do
    key_path = "#{File.expand_path('.')}/test/certs/pixellaru.api.key.pem"
    crt_path = "#{File.expand_path('.')}/test/certs/pixellaru.api.crt.pem"

    api = Gateline::Api.new(key_path, crt_path, 'pixellaru', '1379364a819506583e06628f28a686d3')

    params = {
      merchant_order_id: '1231',
      return_success_url: '',
      return_failure_url: '',
      mobile: 'yes',
      try3d: '1'
    }
    # puts api.activation params
  end

  should 'order info' do
    key_path = "#{File.expand_path('.')}/test/certs/pixellaru.api.key.pem"
    crt_path = "#{File.expand_path('.')}/test/certs/pixellaru.api.crt.pem"

    api = Gateline::Api.new(key_path, crt_path, 'pixellaru', '1379364a819506583e06628f28a686d3')
    res = api.order('55c1aacb15f9d51f9cb10cf7ebf5cbdf')
    puts res['export']['order']['client_card_number']
  end



  should 'rebill' do
    key_path = "#{File.expand_path('.')}/test/certs/pixellaru.api.key.pem"
    crt_path = "#{File.expand_path('.')}/test/certs/pixellaru.api.crt.pem"

    api = Gateline::Api.new(key_path, crt_path, 'pixellaru', '1379364a819506583e06628f28a686d3')

    params = {
      merchant_order_id: '1231',
      amount: '100.0',
      ip: '95.37.19.227',
      cvn: '110',
      mobile: 'yes'
    }
    # puts api.rebill '55c1aacb15f9d51f9cb10cf7ebf5cbdf', params
  end

  should 'rebill 3d' do
    key_path = "#{File.expand_path('.')}/test/certs/pixellaru.api.key.pem"
    crt_path = "#{File.expand_path('.')}/test/certs/pixellaru.api.crt.pem"

    api = Gateline::Api.new(key_path, crt_path, 'pixellaru', '1379364a819506583e06628f28a686d3')

    params = {
      merchant_order_id: '1231',
      amount: '100.0',
      ip: '95.37.19.227',
      cvn: '110',
      mobile: 'yes'
    }
    # puts api.rebill3d '55c1aacb15f9d51f9cb10cf7ebf5cbdf', params
  end

  should 'checkout pay' do
    key_path = "#{File.expand_path('.')}/test/certs/pixellaru.api.key.pem"
    crt_path = "#{File.expand_path('.')}/test/certs/pixellaru.api.crt.pem"

    api = Gateline::Api.new(key_path, crt_path, 'pixellaru', '1379364a819506583e06628f28a686d3')
    params = {
      merchant_order_id: '123',
      amount: '100.00',
      description: 'Тестовый платеж'
    }
    # puts api.checkout_pay params
  end
end
